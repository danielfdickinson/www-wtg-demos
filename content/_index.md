+++
title = "Wild Tech 'Garden' demos top-level placeholder"
description = "A placeholder / pointer to the actual demos and sites at wtg-demos.ca and friends"
summary = "A placeholder / pointer to the actual demos and sites at wtg-demos.ca and friends"
author = "Daniel F. Dickinson"
showChildCards = true
omitDates = true
+++

## Demo sites hosted here

* [Mice: sample of very simple Javascript and CSS](https://www.mice.wtg-demos.ca/)
